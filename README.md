# CI/CD Toolbox Docker Images

CI/CD 파이프라인에 필요한 CLI 명령어를 포함하는 Alpine Linux 기반 Docker 이미지 모음

## Toolbox Images

* [toolbox/ssh](https://gitlab.com/ig-devops-kit/images/container_registry/3711906) : `bash`, `curl`, `jq`, `ssh`, `envsubst`, `yq`
* [toolbox/k8s](https://gitlab.com/ig-devops-kit/images/container_registry/3771455) : `bash`, `curl`, `git`, `jq`, `ssh`, `envsubst`, `yq`, `kubectl`, `kusomize`, `helm`
